const mongoose = require('mongoose');

mongoose.connect(`mongodb+srv://admin:admin123@cluster0.evvkhyz.mongodb.net/S36-Activity?retryWrites=true&w=majority`,{
    useNewUrlParser: true,
    useUnifiedTopology: true
});

let dbConnect = mongoose.connection;

dbConnect.on('error',()=>{
    console.error('connection error');
})
dbConnect.once('open',()=>{
    console.log('Connected to MongoDB');
})

module.exports = dbConnect;