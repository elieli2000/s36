const express = require('express');

const dbConnect = require('./data/database');
const taskRoutes = require('./routes/tasks.routes');

const port = 4000;
const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use('/tasks', taskRoutes);

app.listen(port,()=>console.log(`server is now listening on port ${port}`));